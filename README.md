# Bannière CFA ANV 2023

Bannière temporaire pour la collecte de fin d'année 2023 d'ANV-COP21

## Code HTML 

* Dans l'éditeur de thèmes, modifier `header.php` du thème ANV-COP21 officiel : https://anv-cop21.org/wp-admin/theme-editor.php?file=header.php&theme=anv-cop21
* Inclure le code dans la balise `<header>`
* Mettre à jour le fichier

## Code CSS

* Dans l'éditeur de thèmes, modifier `style.css` du thème ANV-COP21 officiel : https://anv-cop21.org/wp-admin/theme-editor.php?file=style.css&theme=anv-cop21
* Inclure le code dans la feuille de style
* Mettre à jour le fichier

## Voir aussi

https://framagit.org/alternatiba/website/popup-cfa-anv-2023
